package id.ujangwahyu.suitmedia.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ujangwahyu.suitmedia.R;
import id.ujangwahyu.suitmedia.model.GuestModel;

/**
 * Created by Ujang Wahyu on 25/02/2018.
 */

public class GuestAdapter extends RecyclerView.Adapter<GuestAdapter.ViewHolder> {
    List<GuestModel> list;
   // ImageLoader imageLoader;
    public GuestAdapter(List<GuestModel> list) {
        this.list = list;
       // this.imageLoader=imageLoader;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_guest,parent,false);
        ViewHolder myHolder = new ViewHolder(view);
        return myHolder;
    }

    @Override
    public void onBindViewHolder(GuestAdapter.ViewHolder holder, int position) {
        GuestModel guest = list.get(position);
        holder.id.setText(guest.getId());
        holder.name.setText(guest.getName());
        holder.birthdate.setText(guest.getBirthdate());
    }



    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView id, name,birthdate;
        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            id= (TextView) itemView.findViewById(R.id.id);
            name = (TextView) itemView.findViewById(R.id.name);
            birthdate = (TextView) itemView.findViewById(R.id.birthdate);
          //  price= (TextView) itemView.findViewById(R.id.price);

        }


    }

}