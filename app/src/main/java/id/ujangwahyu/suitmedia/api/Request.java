package id.ujangwahyu.suitmedia.api;

import java.util.List;

import id.ujangwahyu.suitmedia.model.GuestModel;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Ujang Wahyu on 25/02/2018.
 */

public interface Request {

    @GET("/api/people")
    Call<List<GuestModel>> viewGuest();

}
