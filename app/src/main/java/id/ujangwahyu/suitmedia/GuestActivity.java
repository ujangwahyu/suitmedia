package id.ujangwahyu.suitmedia;

import android.content.res.Resources;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ujangwahyu.suitmedia.adapter.GuestAdapter;
import id.ujangwahyu.suitmedia.api.Request;
import id.ujangwahyu.suitmedia.model.GuestModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GuestActivity extends AppCompatActivity {
    public static final String URL = "http://dry-sierra-6832.herokuapp.com";
    private List<GuestModel> guestModelList = new ArrayList<>();
    private GuestAdapter guestAdapter;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest);

        recyclerView = (RecyclerView) findViewById(R.id.recycler);

        loadDataGuest();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadDataGuest();
    }

    private void loadDataGuest() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Request api = retrofit.create(Request.class);
        //Call<Value> call = api.viewGuest();
        Call<List<GuestModel>> call = api.viewGuest();

        call.enqueue(new Callback<List<GuestModel>>() {
            @Override
            public void onResponse(Call<List<GuestModel>> call, Response<List<GuestModel>> response) {


                    List<GuestModel> list = response.body();
                    GuestModel result = null;

                    for (int i =0 ;i<list.size();i++){
                        result = new GuestModel();
                        String id = list.get(i).getId();
                        String name = list.get(i).getName();
                        String birthdate = list.get(i).getBirthdate();
                        result.setId(id);
                        result.setName(name);
                        result.setBirthdate(birthdate);
                        guestModelList.add(result);
                    }

                GuestAdapter recyclerAdapter = new GuestAdapter(guestModelList);
                RecyclerView.LayoutManager recyce = new GridLayoutManager(GuestActivity.this,2);
                // RecyclerView.LayoutManager recyce = new LinearLayoutManager(MainActivity.this);
                recyclerView.addItemDecoration(new GridSpacingdecoration(2, dpToPx(10), true));
                recyclerView.setLayoutManager(recyce);
                recyclerView.setItemAnimator( new DefaultItemAnimator());
                recyclerView.setAdapter(recyclerAdapter);


            }

            @Override
            public void onFailure(Call<List<GuestModel>> call, Throwable t) {
                Toast toast = Toast.makeText(GuestActivity.this, "Tidak Dapat akses", Toast.LENGTH_SHORT);
                toast.show();
            }
        });

    }
    public class GridSpacingdecoration extends RecyclerView.ItemDecoration {

        private int span;
        private int space;
        private boolean include;

        public GridSpacingdecoration(int span, int space, boolean include) {
            this.span = span;
            this.space = space;
            this.include = include;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view);
            int column = position % span;

            if (include) {
                outRect.left = space - column * space / span;
                outRect.right = (column + 1) * space / span;

                if (position < span) {
                    outRect.top = space;
                }
                outRect.bottom = space;
            } else {
                outRect.left = column * space / span;
                outRect.right = space - (column + 1) * space / span;
                if (position >= span) {
                    outRect.top = space;
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
