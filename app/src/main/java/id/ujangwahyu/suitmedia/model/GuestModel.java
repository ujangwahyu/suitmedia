package id.ujangwahyu.suitmedia.model;

import java.util.List;

/**
 * Created by Ujang Wahyu on 25/02/2018.
 */

public class GuestModel{
    public String id;
    public String name;
    public String birthdate;
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
    public String getBirthdate() {
        return birthdate;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

}
